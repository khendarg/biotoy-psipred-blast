#!/sw/bin/python

from __future__ import print_function, division
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastpCommandline
import tempfile
import os, re
import quod
from io import BytesIO
import base64


BLASTDB='psipredb_all/blast'
MAXLENGTH = 5000

import cgi, cgitb

def error(*things):
	print('<h1>Error</h1>')
	for thing in things: print(thing, '<br/>')
	print('<a href="javascript:history.back()">Return to form</a><br/>')

def main(sequence, maxhits=5, evalue=0.1, gapopen=11, gapextend=1, comp_based_stats=False):

	tf = tempfile.NamedTemporaryFile()
	tf.write(sequence)
	tf.flush()
	cline = NcbiblastpCommandline(query=tf.name, db='psipred', max_target_seqs=maxhits, evalue=evalue, gapopen=gapopen, gapextend=gapextend, outfmt=5)
	env = os.environ
	env['BLASTDB'] = BLASTDB
	out, err = cline(env=env)
	del tf
	tf = tempfile.NamedTemporaryFile()
	tf.write(out)
	tf.flush()
	tf.seek(0)
	blast = NCBIXML.parse(tf)

	#static_blastpage(blast, sequence=sequence)
	live_blastpage(blast, sequence=sequence)

def print_header(sequence=None):
	print('<html><head><title>List of hits</title></head>')
	print('<style>body { font-family: sans-serif; }\n .row0 { background: #eee; }\n .row1 { background: #ddd; }\n </style>')
	print('<body>')

	print('<div style="width: 50%; overflow: scroll;"><h3>Query sequence:</h2>')
	if sequence is not None: print('<pre>{}</pre>'.format(sequence))
	print('</div><p>')

def print_legend():
	print('<div><h3>Legend</h3>\n')

	print('<table>')
	print('<tr><td style="width: 1em; background: darkred;">&nbsp;</td><td>Helix</td></tr>\n')
	print('<tr><td style="width: 1em; background: darkkhaki;">&nbsp;</td><td>Strand</td></tr>\n')
	print('<tr><td style="width: 1em; background: skyblue;">&nbsp;</td><td>Coil</td></tr>\n')

	print('</div>')



def print_footer():
	print('</body></html>')

def live_blastpage(blast, sequence=None):

	print_header(sequence=sequence)

	print_plot(sequence)


	print('<h3>Hits</h3>\n')
	
	print('<table>')
	print('<tr>')
	print('<th>Subunit</th>')
	print('<th>TCID</th>')
	print('<th>Expect</th>')
	print('<th>Query range</th>')
	print('<th>Target range</th>')
	print('<th>Prediction</th>')
	print('</tr>')

	i = 0
	for record in blast:
		for alignment in record.alignments:
			accession = alignment.accession
			fam = re.sub('\.[0-9]+\.[0-9]+-.*$', '', accession)
			for hsp in alignment.hsps:
				print('<tr class="row{}">'.format(i%2))
				print('<td><a href="http://tcdb.org/search/result.php?tc={}">{}</a></td>'.format(accession[:accession.find('-')], accession[accession.find('-')+1:]))
				print('<td><a href="http://tcdb.org/search/result.php?tc={}">{}</a></td>'.format(fam, accession[:accession.find('-')]))
				print('<td>{:0.1e}</td>'.format(hsp.expect))
				print('<td>{}&ndash;{}</td>'.format(hsp.query_start, hsp.query_end))
				print('<td>{}&ndash;{}</td>'.format(hsp.sbjct_start, hsp.sbjct_end))
				with open('psipredb_all/raws/{}.ss2'.format(accession)) as f:
					predstr = f.read()
				plot = quod.Plot()
				plot.width = 7.5
				plot.height = 2.75
				plot.height = 1.5
				plot.height = 1.5
				plot.grid = False

				what = quod.What(seq=predstr, mode='psipred', window=3)
				psipred = quod.find(what, quod.Psipred)[0]
				psipred.highest = False
				psipred.linewidth = 1.0
				plot.add(what)

				#plot.add(quod.Wall([[hsp.query_start, hsp.query_end]], y=0.85, ylim=[0.5, 1]))
				#plot.add(quod.Wall([[hsp.sbjct_start, hsp.sbjct_end]], y=0.65, ylim=[0, 0.5]))
				plot.add(quod.Wall([[hsp.sbjct_start, hsp.sbjct_end]], y=0.75, ylim=[0, 1]))

				plot.render()
				plot.ax.set_title(accession)
				plot.ax.set_title('')
				plot.ax.set_xlabel('')
				plot.ax.set_ylabel('')
				plot.ax.set_yticks(quod.np.arange(0.4, 1.2, 0.2))
				plot.ax.set_ylim(bottom=0.5, top=1.0)
				xticks = plot.ax.get_xticks()
				xlim = plot.ax.get_xlim()
				spacing = (xticks[1] // (20 * 2)) * 20
				plot.ax.set_xticks(quod.np.arange(xticks[0], xticks[-1]+spacing, spacing))
				plot.ax.set_xlim(xlim)
				rawout = BytesIO()
				plot.save(rawout, dpi=80, format='png')
				rawout.seek(0)
				#python3 note: use base64.encodebytes instead
				encoded = base64.binascii.b2a_base64(rawout.read())
				print('<td><a href="tcpred.py?tc={}&start={}&end={}"><img src="data:image/png;base64,{}" /></a></td>'.format(accession, hsp.sbjct_start, hsp.sbjct_end, encoded))

	

				#print('<td><a href="psipredb_all/window1/{}.png"><img src="psipredb_all/window1/{}.png" height="144pt"/></a></td>'.format(accession, accession))
				print('</tr>')
				i += 1

	print('</table>')
	print_footer()

def print_plot(sequence):
	plotwidth = 8
	plotheight = 3
	print('<h3>Query hydropathy (blue)/amphipathy (red) plot</h3>')
	plot = quod.Plot()

	plot.width = 8
	plot.height = 3
	hydro = quod.Hydropathy(sequence, style='b', index=quod.HYDROPATHY)
	amphi = quod.Hydropathy(sequence, style='r', index=quod.AMPHIPATHICITY)
	hmmtop = quod.HMMTOP(sequence)
	plot.add(hydro)
	plot.add(amphi)
	plot.add(hmmtop)

	plot.render()
	plot.ax.set_title('')

	xticks = plot.ax.get_xticks()
	xlim = plot.ax.get_xlim()
	#spacing = max(20, (xlim[1] // (20 * 2)) * 20)
	spacing = max(20, (xticks[1] // (20 * 2)) * 20)
	plot.ax.set_xticks(quod.np.arange(xticks[0], xticks[-1] + spacing, spacing))
	plot.ax.set_xlim(xlim)
	rawout = BytesIO()

	plot.save(rawout, dpi=300, format='png')
	rawout.seek(0)

	encoded = base64.binascii.b2a_base64(rawout.read())
	print('<img width="{}pt" src="data:image/png;base64,{}" /><br/>'.format(plotwidth*96, encoded))
	
def static_blastpage(blast, sequence=None):

	print_header(sequence=sequence)

	print_plot(sequence)

	print('Legend<br/><table><tr><td>SS prediction</td><td>Color</td></tr>')
	print('<tr><td>Helix</td><td>Red</td></tr>')
	print('<tr><td>Strand</td><td>Yellow</td></tr>')
	print('<tr><td>Coil</td><td>Blue</td></tr>')

	print('<table>')
	print('<tr>')
	print('<th>Subunit</th>')
	print('<th>TCID</th>')
	print('<th>Expect</th>')
	print('<th>Query range</th>')
	print('<th>Target range</th>')
	print('<th>Prediction</th>')
	print('</tr>')

	i = 0
	for record in blast:
		for alignment in record.alignments:
			accession = alignment.accession
			fam = re.sub('\.[0-9]+\.[0-9]+-.*$', '', accession)
			for hsp in alignment.hsps:
				print('<tr class="row{}">'.format(i%2))
				print('<td><a href="http://tcdb.org/search/result.php?tc={}">{}</a></td>'.format(accession[:accession.find('-')], accession[accession.find('-')+1:]))
				print('<td><a href="http://tcdb.org/search/result.php?tc={}">{}</a></td>'.format(fam, accession[:accession.find('-')]))
				print('<td>{:0.1e}</td>'.format(hsp.expect))
				print('<td>{}&ndash;{}</td>'.format(hsp.query_start, hsp.query_end))
				print('<td>{}&ndash;{}</td>'.format(hsp.sbjct_start, hsp.sbjct_end))
				print('<td><a href="psipredb_all/window1/{}.png"><img src="psipredb_all/window1/{}.png" height="144pt"/></a></td>'.format(accession, accession))
				print('</tr>')
				i += 1
			#for hsp in alignment.hsps:
			#	print(dir(hsp))
	print('</table>')

	print_footer()

	#print(out)

if __name__ == '__main__':
	print('Content-type: text/html\r\n\r\n')
	form = cgi.FieldStorage()

	sequence = form.getvalue('Sequence')
	if sequence is None: error('Please enter a sequence')
	elif not len(sequence.strip()): error('Please enter a sequence')
	elif len(sequence) > MAXLENGTH: error('Sequence is too long (&gt;%d)' % MAXLENGTH)

	maxhits = form.getvalue('maxhits')
	try: maxhits = min(25, int(maxhits))
	except ValueError: maxhits = 5
	except TypeError: maxhits = 5

	evalue = form.getvalue('evalue')
	try: evalue = float(evalue)
	except ValueError: evalue = 0.1
	except TypeError: evalue = 0.1

	gapopen = form.getvalue('gapopen')
	try: gapopen = int(gapopen)
	except ValueError: gapopen = 11
	except TypeError: gapopen = 11

	gapextend = form.getvalue('gapextend')
	try: gapextend = int(gapextend)
	except ValueError: gapextend = 1
	except TypeError: gapextend = 1

	if form.getvalue('comp_based_stats').startswith('y'): comp_based_stats = True
	else: comp_based_stats = False

	max_hsps = form.getvalue('max_hsps')
	try: max_hsps = int(max_hsps)
	except ValueError: max_hsps = 1
	except TypeError: max_hsps = 1

	main(sequence, maxhits=maxhits, evalue=evalue, gapopen=gapopen, gapextend=gapextend, comp_based_stats=comp_based_stats)


