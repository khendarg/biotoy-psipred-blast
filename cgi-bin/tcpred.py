#!/sw/bin/python

from __future__ import print_function, division
import tempfile
import os, re
import quod
from io import BytesIO
import base64
#import psipred_blast

BLASTDB='psipredb_all/blast'
import subprocess

import cgi, cgitb

def print_header(accession):
	print('<html>')
	print('<head>')
	print('<title>TCPRED: {}</title>'.format(accession))
	print('<style>body { font-family: sans-serif }</style>')
	print('</head>')
	print('<body>')

def print_psipred_plot(accession, span=None):
	plotwidth = 8
	plotheight = 3
	print('<h3>Full PSIPRED plot for {}</h3>'.format(accession))
	plot = quod.Plot()
	plot.width = plotwidth
	plot.height = plotheight
	with open('psipredb_all/raws/{}.ss2'.format(accession)) as f:
		predstr = f.read()
	what = quod.What(seq=predstr, mode='psipred', window=1)
	psipred= quod.find(what, quod.Psipred)[0]
	psipred.linewidth = 1
	plot.add(what)

	if span is not None: plot.add(quod.Wall(spans=[span], y=0.5, ylim=[0,1]))

	plot.render()
	plot.ax.set_title(accession)
	plot.ax.set_yticks(quod.np.arange(0., 1., 0.1))
	xticks = plot.ax.get_xticks()
	xlim = plot.ax.get_xlim()
	spacing = max(20, (xticks[1] // (20 * 2)) * 20)
	plot.ax.set_xticks(quod.np.arange(xticks[0], xticks[-1] + spacing, spacing))
	plot.ax.set_xlim(xlim)
	rawout = BytesIO()

	plot.save(rawout, dpi=300, format='png')
	rawout.seek(0)

	#python3 note: use base64.encodebytes instead
	encoded = base64.binascii.b2a_base64(rawout.read())
	print('<img width="{}pt" src="data:image/png;base64,{}" /><br/>'.format(plotwidth*96, encoded))

def print_hydropathy_plot(accession, seq, span=None):
	plotwidth = 8
	plotheight = 3


	print('<h3>Hydropathy plot for {}</h3>'.format(accession))


	plot = quod.Plot()
	plot.width = plotwidth
	plot.height = plotheight
	hydro = quod.Hydropathy(gseq=seq, style='b', window=19, index=quod.HYDROPATHY)
	amphi = quod.Hydropathy(gseq=seq, style='r', window=19, index=quod.AMPHIPATHICITY)
	hmmtop = quod.HMMTOP(gseq=seq)
	plot.add(hydro)
	plot.add(amphi)
	plot.add(hmmtop)

	if span is not None: plot.add(quod.Wall(spans=[span], y=0.0, ylim=[0,1]))

	plot.render()
	plot.ax.set_title(accession)

	xticks = plot.ax.get_xticks()
	xlim = plot.ax.get_xlim()
	#spacing = max(20, (xlim[1] // (20 * 2)) * 20)
	spacing = max(20, (xticks[1] // (20 * 2)) * 20)
	plot.ax.set_xticks(quod.np.arange(xticks[0], xticks[-1] + spacing, spacing))
	plot.ax.set_xlim(xlim)
	rawout = BytesIO()

	plot.save(rawout, dpi=300, format='png')
	rawout.seek(0)

	#python3 note: use base64.encodebytes instead
	encoded = base64.binascii.b2a_base64(rawout.read())
	print('<img width="{}pt" src="data:image/png;base64,{}" /><br/>'.format(plotwidth*96, encoded))

def print_entropy_plot(accession, seq, span=None):
	plotwidth = 8
	plotheight = 3


	print('<h3>Entropy plot for {}</h3>'.format(accession))


	plot = quod.Plot()
	plot.width = plotwidth
	plot.height = plotheight
	entropy = quod.Entropy(gseq=seq, window=19)
	hmmtop = quod.HMMTOP(gseq=seq)
	plot.add(entropy)
	plot.add(hmmtop)

	if span is not None: plot.add(quod.Wall(spans=[span], y=3.5, ylim=[0,1]))

	plot.render()
	plot.ax.set_title(accession)

	xlim = plot.ax.get_xlim()
	xticks = plot.ax.get_xticks()
	#spacing = max(20, (len(seq) // (20 * 2)) * 20)
	spacing = max(20, (xticks[1] // (20 * 2)) * 20)
	plot.ax.set_xticks(quod.np.arange(xticks[0], xticks[-1] + spacing, spacing))
	plot.ax.set_xlim(xlim)
	rawout = BytesIO()

	plot.save(rawout, dpi=600, format='png')
	rawout.seek(0)

	#python3 note: use base64.encodebytes instead
	encoded = base64.binascii.b2a_base64(rawout.read())
	print('<img width="{}pt" src="data:image/png;base64,{}" /><br/>'.format(plotwidth*96, encoded))


def print_footer():
	print('</body></html>')

def main(tcid, span=None):
	print_header(tcid)

	print_psipred_plot(tcid, span=span)

	cmd = ['blastdbcmd', '-db', 'psipred', '-entry', tcid]
	env = os.environ
	env['BLASTDB'] = BLASTDB
	out = subprocess.check_output(cmd, env=env)
	

	print_hydropathy_plot(tcid, seq=out, span=span)
	print_entropy_plot(tcid, seq=out, span=span)

	print_footer()

if __name__ == '__main__':
	print('Content-type: text/html\r\n\r\n')
	form = cgi.FieldStorage()
	try: tcid = form.getvalue('tc')
	except ValueError: psipred_blast.error('Please specify a TCID')
	try: start = int(form.getvalue('start'))
	except ValueError: start = None
	try: end = int(form.getvalue('end'))
	except ValueError: end = None

	if start is not None and end is not None:
		span = [start, end]
	else: span = None

	main(form.getvalue('tc'), span=span)
